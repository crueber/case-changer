
const changeCase = require('change-case');

const falsey = [undefined, null];
const formatObjectKeys = (switchTo, obj, doValues = true) => {
  if (!changeCase[switchTo]) throw new Error(`${switchTo} is an unknown case changer`);
  if (typeof obj !== 'object' || falsey.includes(obj)) {
    return typeof obj === 'string' && doValues ? changeCase[switchTo](obj) : obj;
  }
  if (Array.isArray(obj)) return obj.map(i => formatObjectKeys(switchTo, i, false));

  return Object.keys(obj).reduce((newObj, key) => {
    newObj[changeCase[switchTo](key)] = formatObjectKeys(switchTo, obj[key], false);
    return newObj;
  }, {});
};

module.exports = formatObjectKeys;
