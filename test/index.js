const { expect } = require('chai');

const formatter = require('..');

describe('formatter', () => {
  it('should transform a string', () => {
    const updated = formatter('snake', 'thisThing');
    expect(updated).to.equal('this_thing');
  })
  it('should transform an object', () => {
    const updated = formatter('snake', {a:1,bC:2});
    expect(updated).to.deep.equal({a:1,b_c:2});
  })
  it('should transform an array', () => {
    const updated = formatter('snake', ['a', {bC:2}]);
    expect(updated).to.deep.equal(['a', {b_c:2}]);
  })
  it('should transform a nested object', () => {
    const updated = formatter('snake', {aA:{bB:{cC:3}}});
    expect(updated).to.deep.equal({a_a:{b_b:{c_c:3}}});
  })
  it('should not transform values in objects', () => {
    const updated = formatter('snake', {aA:'aB cDD Dc'});
    expect(updated).to.deep.equal({a_a:'aB cDD Dc'});
  })
})
